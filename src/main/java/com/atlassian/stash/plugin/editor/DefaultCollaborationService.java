package com.atlassian.stash.plugin.editor;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.stash.plugin.editor.ao.RepoCollabAllowed;
import com.atlassian.stash.repository.Repository;
import net.java.ao.DBParam;
import net.java.ao.Query;

import java.util.Collections;

public class DefaultCollaborationService implements CollaborationService {
    private final ActiveObjects ao;

    public DefaultCollaborationService(ActiveObjects ao) {
        this.ao = ao;
    }

    public boolean isEnabledForRepository(Repository repository) {
        final Integer repoId = repository.getId();
        return ao.executeInTransaction(new TransactionCallback<Boolean>() {
            @Override
            public Boolean doInTransaction() {
                return ao.get(RepoCollabAllowed.class, repoId) != null;
            }
        });
    }

    public void setEnabledForRepository(final Repository repository, final boolean enabled) {
        final Integer repoId = repository.getId();
        ao.executeInTransaction(new TransactionCallback<Void>() {
            @Override
            public Void doInTransaction() {
                RepoCollabAllowed repoEnabled = ao.get(RepoCollabAllowed.class, repoId);
                boolean requiresUpdate = enabled == (repoEnabled == null);
                if (requiresUpdate) {
                    if (enabled) {
                        ao.create(RepoCollabAllowed.class, new DBParam("REPOSITORY_ID", repoId));
                    } else {
                        ao.delete(repoEnabled);
                    }
                }
                return null;
            }
        });
    }
}
