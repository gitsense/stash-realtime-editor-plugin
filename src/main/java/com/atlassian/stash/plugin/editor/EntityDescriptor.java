package com.atlassian.stash.plugin.editor;

import com.atlassian.stash.repository.Repository;

public class EntityDescriptor {
    private String at;
    private String path;
    private Repository repository;

    public EntityDescriptor(String at, String path, Repository repository) {
        this.at = at;
        this.path = path;
        this.repository = repository;
    }

    public String getAt() {
        return at;
    }

    public String getPath() {
        return path;
    }

    public Repository getRepository() {
        return repository;
    }
}
