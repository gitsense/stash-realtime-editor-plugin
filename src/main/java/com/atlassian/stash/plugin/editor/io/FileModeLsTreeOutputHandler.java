package com.atlassian.stash.plugin.editor.io;

import com.atlassian.stash.io.LineReader;
import com.atlassian.stash.io.LineReaderOutputHandler;
import com.atlassian.stash.scm.CommandOutputHandler;

import javax.annotation.Nullable;
import java.io.IOException;

/**
 * Parse filemode for a single entry in a tree.
 * <p/>
 * Example command and output:
 *
 * <pre>
 * $ git ls-tree HEAD:
 * 100644 blob 18f044355767fe3f5a62b9da488d20aee27a20a8	.gitignore
 * 100644 blob 9c6ab63f0d7e04ab1ca5c4045f58fa128584afdf	LICENSE
 * 100644 blob 94e6a43d33d8a35ae264bff4234ca86869a68c4c	README
 * 100644 blob 4c3882e2843a1bb27822ca68a8140aeb1ab3f141	pom.xml
 * 040000 tree 268c62d410747d6bb13cabdcc80f31744929d7da	src
 * </pre>
 */
public class FileModeLsTreeOutputHandler extends LineReaderOutputHandler implements CommandOutputHandler<String> {

    private final String filename;

    private volatile String fileMode;

    public FileModeLsTreeOutputHandler(String filename) {
        super("UTF-8");
        this.filename = filename;
    }

    @Nullable
    @Override
    public String getOutput() {
        return fileMode;
    }

    @Override
    protected void processReader(LineReader lineReader) throws IOException {
        String line;
        while ((line = lineReader.readLine()) != null) {
            // output format: <mode> SP <type> SP <object> TAB <file>
            String path = line.substring(line.indexOf('\t') + 1);
            if (filename.equals(path)) {
                fileMode = line.substring(0, line.indexOf(' '));
                break;
            }
        }
    }

}
